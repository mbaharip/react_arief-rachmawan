# 30 - Weekly 2

Task section ini sebagai berikut:

1. Lakukan installation framework / library yang dipelajari, disini saya menggunakan React JS
2. Lakukan installation framework CSS, disini saya menggunakan TailwindCSS
3. Buatlah sebuah halaman yang berisikan rangkuman dari task yang telah dikerjakan sebelumnya
4. Buat routing pada halaman tersebut dan gunakan sidebar untuk melakukan perpindahan route
5. Harus ada title yang menggambarkan setiap materi yang dipelajari
6. Masukkan deskripsi untuk setiap title
7. Masukkan screenshot untuk hasil pengerjaan task
8. Lakukan deploy untuk halaman yang telah dibuat
9. Penggunaan global state merupakan nilai tambah
10. Jika dapat memasukkan project kalian dalam setiap root juga akan menjadi nilai tambah

Berikut hasil task yang saya kerjakan:  
[Github Repository](https://github.com/mbahArip/Assignment-Weekly-2)  
[Netlify Deploy](https://ariefrachmawan-reacta-weekly2.netlify.app/)  
![Home](./screenshots/home.png)
![Task](./screenshots/task.png)
![404](./screenshots/404.png)
