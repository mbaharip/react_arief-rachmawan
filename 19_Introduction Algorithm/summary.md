# 19 Introduction Algorithm

## Resume

Pada materi ini saya mempelajari :

-   Algoritma
-   Pseudo-code
-   Flowchart

### Algoritma

Algoritma adalah prosedur komputasi yang didefinisikan dengan baik yang mengambil beberapa nilai sebagai input dan menghasilkan beberapa nilai sebagai output.

Contoh algoritma:

-   Mencari bilangan prima
-   Sorting
-   Searching

Karakteristik algoritma:

-   Memiliki batas
-   Instruksi terdefinisi dengan baik
-   Efektif dan Efisien

Algoritma terbagi menjadi 3 jenis:

-   Sequential, adalah algoritma yang menggunakan langkah-langkah yang terdefinisi secara berurutan.
-   Branching, adalah algoritma yang menggunakan langkah-langkah yang terdefinisi secara berbeda-beda.
-   Looping, adalah algoritma yang menggunakan langkah-langkah yang terdefinisi secara berulang-ulang.

### Pseudo-code

Pseudo-code adalah kode yang dibuat untuk menjelaskan algoritma. Pseudo-code dapat dibahas secara lebih jelas dengan menggunakan kode yang lebih mudah dibaca.

### Flowchart

Flowchart adalah gambar yang menggambarkan algoritma. Flowchart dapat dibahas secara lebih jelas dengan menggunakan gambar yang lebih mudah dibaca.

Simbol-simbol yang digunakan:

-   Oval, digunakan untuk menandai start atau end pada sebuah flowchart
-   Kotak, digunakan untuk menandai proses
-   Jajar genjang, digunakan untuk menandai input / output
-   Belah ketupat, digunakan untuk menandai kondisi

---

## Task

Pada task kali ini saya harus membuat flowchart dari 2 masalah, yaitu:

1. Mencari bilangan prima
2. Lampu dan Tombol

Berikut hasil dari task kali ini:  
Flowchart dapat dilihat di [Whimsical](https://whimsical.com/flowchart-CJoApY16dywZ4EcboeG79G)

1. Mencari bilangan prima  
   ![Prime number](./screenshots/prime.png)
